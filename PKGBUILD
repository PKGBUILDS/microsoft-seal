# Maintainer: ObserverOfTime <chronobserver@disroot.org>
# Based on sealcrypto

pkgname=microsoft-seal
pkgver=4.1.1
pkgrel=1
pkgdesc='Microsoft library for fully homomorphic encryption'
arch=('x86_64')
url="https://sealcrypto.org/"
license=('MIT')
makedepends=('clang' 'cmake' 'microsoft-gsl' 'zlib' 'zstd-static')
optdepends=(
  'microsoft-gsl: API extensions'
  'zlib: Compressed serialization'
  'zstd: Faster compressed serialization'
)
conflicts=('sealcrypto')
provides=("sealcrypto=${pkgver}")
source=("SEAL-${pkgver}.tar.gz::https://github.com/microsoft/SEAL/archive/${pkgver}.tar.gz")
sha256sums=('af9bf0f0daccda2a8b7f344f13a5692e0ee6a45fea88478b2b90c35648bf2672')

build() {
  cd SEAL-${pkgver}
  cmake -B build \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_CXX_COMPILER=clang++ \
    -DCMAKE_C_COMPILER=clang \
    -DSEAL_USE_MSGSL=ON \
    -DSEAL_BUILD_DEPS=OFF .
  cmake --build build
}

package() {
  cd SEAL-${pkgver}
  DESTDIR="${pkgdir}" cmake --install build
  install -Dm644 LICENSE \
      "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
